test = "Hello World!"

print(test.lower())
print(test.upper())

print(test.isalpha())  # False because of ' ' and !
print("continuousString".isalpha())  # True
print("l3tt3r54ndNumber5".isalnum())
print("35".isdecimal())
print("    ".isspace())
print(test.istitle())
print(test.startswith("Hel"))
print(test.endswith("d!"))

# join function
dummy_list = ["cat", "rat", "bat"]
res = ",".join(dummy_list)
print(res)
res = " ".join(dummy_list)
print(res)
print("   x   ".strip())
print("   x   a".lstrip())
print("   x   ".rstrip())
