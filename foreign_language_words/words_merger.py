import json
from recordtype import recordtype
import openpyxl


DUOLINGO = 'duolingo'
MOST_1000 = 'most_1000'
VOCABULARY_TAG = 'vocab_overview'
WORD_TAG = 'word_string'
POS_TAG = 'pos'
GENDER_TAG = 'gender'
ID_TAG = 'id'
GERMAN_TAG = 'german'
ENGLISH_TAG = 'english'

Word = recordtype('Word', 'german english part_of_speech sources')
words = dict()

# add duolingo words
with open('words.json') as json_file:
    duolingo_words = json.load(json_file)

for word in duolingo_words[VOCABULARY_TAG]:
    if word[WORD_TAG] not in words:
        words[word[WORD_TAG].lower()] = Word('', '', '', [])

    current_word = words[word[WORD_TAG]]
    current_word.german = word[WORD_TAG].lower()
    current_word.english = ''
    if word[POS_TAG] and word[POS_TAG].lower() == 'noun':
        if not word[GENDER_TAG]:  # some nouns don't have gender
            continue
        current_word.part_of_speech = word[POS_TAG] + '_' + word[GENDER_TAG]
    else:
        current_word.part_of_speech = word[POS_TAG]

    current_word.sources.append('duolingo')

# add most common 1000 words
with open('1000_most_common.json') as json_file:
    most_common = json.load(json_file)

for word in most_common:
    if word[GERMAN_TAG].lower() not in words:
        words[word[GERMAN_TAG].lower()] = Word('', '', '', [])

    current_word = words[word[GERMAN_TAG].lower()]
    current_word.german = word[GERMAN_TAG]
    current_word.english = word[ENGLISH_TAG]
    current_word.sources.append('1000')

all_parts = set()
for word in words.values():
    all_parts.add(word.part_of_speech)

wb = openpyxl.Workbook()
for part in all_parts:
    if not part or part == '':  # due to 1000 most common not having part fo speech
        continue
    wb.create_sheet(part)

for word in words.values():
    if not word.part_of_speech or word.part_of_speech == '':
        current_sheet = wb['Sheet']
    else:
        current_sheet = wb[word.part_of_speech]
    current_sheet.append([word.german, word.english, str(word.sources)])

wb.save('words.xlsx')




