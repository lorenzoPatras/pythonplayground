import argparse
import requests
import json

FILE_NAME = 'words.json'
VOCABULARY_TAG = 'vocab_overview'
WORD_TAG = 'word_string'
POS_TAG = 'pos'
GENDER_TAG = 'gender'
ID_TAG = 'id'


def parse_arguments():
    parser = argparse.ArgumentParser(description='Gets Duolingo word list')
    parser.add_argument('--username', '-u', help='Username')
    parser.add_argument('--password', '-p', help='Password')
    parser.add_argument('--language', '-l', help='Language [e.g.: de for German]')
    args = parser.parse_args()
    return args.username, args.password, args.language


def perform_login(session, username, password):
    r = session.post("https://www.duolingo.com/login", params={"login": username, "password": password})
    if r.status_code != 200:
        print("Login failed with status code: {status_code}. Message:{message}".format(status_code=r.status_code,
                                                                                       message=r.text))


def select_language(session, language):
    r = session.post("https://www.duolingo.com/switch_language", params={"learning_language": language})
    if r.status_code != 200:
        print("Language switch to{language} failed with status code: {status_code}. Message:{message}".format(
            language=language, status_code=r.status_code, message=r.text))


def get_words(session):
    r = session.get("https://www.duolingo.com/vocabulary/overview")
    if r.status_code != 200:
        print("Get words failed with status code: {status_code}. Message:{message}".format(status_code=r.status_code,
                                                                                           message=r.text))
    json_data = r.json()
    return json_data


def save_words_to_file(words):
    with open(FILE_NAME, 'w') as outfile:
        json.dump(words, outfile, indent=2)


def download_words():
    session = requests.Session()
    try:
        perform_login(session, username, password)
        select_language(session, language)
        words = get_words(session)
        save_words_to_file(words)
    except Exception as e:
        print("Caught general exception: " + str(e))


if __name__ == '__main__':
    username, password, language = parse_arguments()
    download_words()
