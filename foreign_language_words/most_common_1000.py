# run with --link=https://1000mostcommonwords.com/1000-most-common-german-words/
import argparse
import requests
import json
from bs4 import BeautifulSoup

TABLE_TAG = 'table'
ENGLISH = 'english'
GERMAN = 'german'
FILE_NAME = '1000_most_common.json'

def parse_arguments():
    parser = argparse.ArgumentParser(description='Gets 1000 most common words content')
    parser.add_argument('--link', '-l', help='link')
    args = parser.parse_args()
    return args.link


def get_words_table(link):
    page = requests.get(link)
    if page.status_code != 200:
        print('request to page link was unsuccessful')
        return

    soup = BeautifulSoup(page.content, 'html.parser')
    tables = soup.find_all(TABLE_TAG)
    if len(tables) == 0:
        print('no table was found')
        return

    return tables[0]


def parse_table(table):
    words = []
    for row in table.findAll('tr')[1:]: #skip header
        cols = row.findAll('td')
        words.append({
            GERMAN: cols[1].getText(),
            ENGLISH: cols[2].getText()
        })
    return words


def save_words_to_file(words):
    with open(FILE_NAME, 'w') as outfile:
        json.dump(words, outfile, indent=2)


if __name__ == '__main__':
    link = parse_arguments()
    table = get_words_table(link)
    words = parse_table(table)
    save_words_to_file(words)

