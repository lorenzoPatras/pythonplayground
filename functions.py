import pyperclip

pyperclip.copy("Dummy text in clipboard")
clip_str = pyperclip.paste()
print(clip_str)


def dummy_function(input_arg):
    print(input_arg)
    return 0


ret_value = dummy_function(5)
print(ret_value)


def div5by(arg):
    try:
        return 5 / arg
    except ZeroDivisionError:
        print("Tried to divide by 0")


print(div5by(3))
print(div5by(2))
print(div5by(0))