import webbrowser
import requests
import pprint
import json
# import bs4  # beautiful soup helps parsing html pages TODO
# to control (automate test) something on web pages use selenium module

# webbrowser.open("https://www.google.com/maps")  # launches the webbrowser with the address
# this can be usefull when you want to append in adrress some other strings and open specific pages

# requests
pp = pprint.PrettyPrinter(indent=2)

# get all users
resp = requests.get("https://reqres.in/api/users")
print("Get all users status: " + str(resp.status_code))
pp.pprint(json.loads(resp.text))

# get user by id
resp = requests.get("https://reqres.in/api/users/{id}".format(id=2))
pp.pprint(json.loads(resp.text))

# create new user
new_user = {"name": "ion", "job": "unempl"}
resp = requests.post("https://reqres.in/api/users/", data=new_user)
print("Create user status: " + str(resp.status_code))  # 201 = created
pp.pprint(json.loads(resp.text))

# update user
update_user = {"name": "ion", "job": "unemployed"}
resp = requests.put("https://reqres.in/api/users/", data=update_user)
print("Create update user status: " + str(resp.status_code))
pp.pprint(json.loads(resp.text))

# patch
update_user = {"name": "ion", "job": "unemployed patched"}
resp = requests.patch("https://reqres.in/api/users/", data=update_user)
print("Create update user status: " + str(resp.status_code))
pp.pprint(json.loads(resp.text))

# delete
resp = requests.delete("https://reqres.in/api/users/{id}".format(id=2))
print("Create update user status: " + str(resp.status_code))  # 204 deleted
