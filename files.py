import os
import shelve
import shutil

# when specify paths take care at the \ separator for windows which is also escape character @_@
# I prefer using raw strings r"path\to\folder" which are interpreted as they are without escape chars in order to avoid
# constructions like "path\\to\\the\\folder"

result = os.path.join("folder1", "folder2", "file")
print("Join result: " + result)
print("OS separator: " + os.sep)  # separator that os uses to concatenate path parts
print("CWD: " + os.getcwd())  # CurrentWorkingDirectory
# os.chdir(r"c:\") - changes the working directory

print("Absolute path: " + os.path.abspath(
    r"files.py"))  # gives you the absolute path to a file you reference by relative path
print("Is absolute path: " + str(os.path.isabs(r"..\..\..\test")))
print("Is absolute path: " + str(os.path.isabs(r"c:\users\test")))
print("Exists path: " + str(os.path.exists(r"c:\users\test")))
print("Is directory: " + str(os.path.isdir(r"..\pythonplayground")))
print("Is file: " + str(os.path.isfile(r"..\pythonplayground\regex.py")))
print("Get file size: " + str(os.path.getsize(r"..\pythonplayground\regex.py")))  # size in bytes
print("List dir: " + str(os.listdir()))
print("List dir: " + str(os.listdir("..\\")))  # for some reason raw strings don't work with this

# if not os.path.isdir(r"dummy_dir"):
#     os.makedirs("dummy_dir")
#
# if os.path.exists(r"dummy_dir"):
#     os.rmdir("dummy_dir")

# write a text file
# I prefer the with syntax for short code because it also calls file.close() at the end
with open("test.txt", "w") as write_file:
    write_file.write("first line\n")
    write_file.write("second line\n")
    write_file.write("third line\n")
    write_file.writelines(["fourth line\n", "fifth_line\n", "sixth_line\n"])

with open("test.txt", "r") as read_file:
    print(read_file.read())
    '''
    after a read is performed, if you call again read it will do nothing because the "cursor" is at the end of the file.
    Use seek(0) to move th cursor on the first position
    '''
    read_file.seek(0)
    print(read_file.readlines())  # use this to parse a file line by line


# if you need to store dicts in files use shelve library
shelfeFile = shelve.open("dummy_file")
shelfeFile["cats"] = ["Zophie", "Pooka", "Cleo"]
shelfeFile.close()

shelfeFile = shelve.open("dummy_file")
print(shelfeFile["cats"])


# copy paste files
if not os.path.isdir(r"dummy_dir"):
    os.makedirs("dummy_dir")
shutil.copy(r"test.txt", r"dummy_dir")
# shutil.copytree()  # recursive copy
shutil.move(r"dummy_file.bak", r"dummy_dir")
shutil.rmtree(r"dummy_dir")  # removes it even if it is not empty
os.unlink(r"test.txt")  # delete a file
os.unlink(r"dummy_file.dat")
os.unlink(r"dummy_file.dir")

# parsing folders recursively
# for folderName, subfolders, filenames in os.walk("folder_name):
#   folderName = the folder on which the walk starts
#   subfolders = list of all subfolders in folderName
#   filenames = list of all files in folderName