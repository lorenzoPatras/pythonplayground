condition = True # False

if condition:
    print("true")
else:
    print("false")

# strings are falsey values
dummy_str = ""
if not dummy_str:
    print("String is empty")

# while loop - print numbers div by 3 < 100
nr = 0
while nr < 100:
    if nr % 3 == 0:
        print(nr)

    nr += 1

# for loop - print numbers div by 7 < 100
for i in range(100): # range(100) = [0..99]; range (5, 10) = [5..9]
    if i % 7 == 0:
        print(i)
