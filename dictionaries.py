dummy_dict = {
    "name": "carl",
    "age": 8,
    "species": "human"
}

print(dummy_dict)
print(dummy_dict["name"])
print(dummy_dict.keys())  # to get a list of keys: list(dummy_dict.keys())
print(dummy_dict.values())
print(dummy_dict.items())

for key in dummy_dict.keys():
    print("key: " + str(key) + " - value: " + str(dummy_dict[key]))  # it still amazes me how a high level language as python cannot default to string the basic types

print(dummy_dict.get("height", 0))  # of there is no key "height" it will return the default value (0 in this case)
print(dummy_dict.get("age", 0))

dummy_dict.setdefault("color", "black")  # if the key "color" is not set it will set it to black
print(dummy_dict)
dummy_dict.setdefault("color", "orange")  # will not set it anymore because now "color" is already set
print(dummy_dict)

# pprint module to pretty print
import pprint
pp = pprint.PrettyPrinter(indent=2)
pp.pprint(dummy_dict)  # will print in ascending order of the keys
