# slice example
dummy_list= ["a", "b", "c", "d", "e", "F"]
print(dummy_list[1:3])
print(dummy_list[-1])  # print last elemnt
print(dummy_list[2:])  # all elements from index 2 onwards
print(dummy_list[:3])  # elements from 0..3
print(len(dummy_list))

# different types in a list
second_list = [1, 3, "a", [4, "b", 6]]
print(second_list)

concat_list = dummy_list + second_list
print(concat_list)

dummy_str = "this is a string to be transformed to a list"
print(list(dummy_str))

# multiple assignment
cat = ["fat", "grey", "loud"]
size, color, disposition = cat
print(size)
print(color)
print(disposition)

# iterate through list
for elem in dummy_list:
    print("separate elem " + str(elem))

# main functions on lists
nr_list = [1, 3, 6, 4, 2, 6, 7]
print(nr_list)
print(nr_list.index(3))  # prints the index where it finds elem 3
nr_list.append(10)
print(nr_list)
nr_list.insert(2, 20)  # insert at index 2, elem 20
print(nr_list)
nr_list.remove(6)
print(nr_list)  # removes the first occurence of 6
nr_list.sort()
print(nr_list)