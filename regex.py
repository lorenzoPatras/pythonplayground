import re

# use raw strings r"" with regex because many of the contain \ and you don't want them to be considered escape char
reg = re.compile(r"(\d\d\d)")  # find the first occurence of 6 digits and split them in groups of 3
res = reg.search("blabla123456bla")
print(res.group())  # 123456
print(res.group(1))  # 123
print(reg.findall("blabla123456bla"))  # ["123", "456"]

# pipe
reg1 = re.compile(r"Bat(man|mobile)")  # searches ony for words Batman and Batmobile
res = reg1.search("Batman lost his mobile")
print(res.group())  # Batman

# patterns
# ? - match zero or one
batReg = re.compile(r"Bat(wo)?man")
res = batReg.search("Adventures of Batman")
print(res.group())  # Batman
res = batReg.search("Adventures of Batwoman")
print(res.group())  # Batwoman
res = batReg.search("Adventures of Batwowowoman")
print(res == None)  # True

# * - zero or more times
batReg = re.compile(r"Bat(wo)*man")
res = batReg.search("Adventures of Batman")
print(res.group())  # Batman
res = batReg.search("Adventures of Batwoman")
print(res.group())  # Batwoman
res = batReg.search("Adventures of Batwowowoman")
print(res == None)  # False
print(res.group())  # Batwowowoman

# + - one or more
batReg = re.compile(r"Bat(wo)+man")
res = batReg.search("Adventures of Batman")
print(res == None)  # True
res = batReg.search("Adventures of Batwoman")
print(res.group())  # Batwoman
res = batReg.search("Adventures of Batwowowoman")
print(res.group())  # Batwowowoman

# {x} - exactly x times
reg = re.compile(r"(Ha){3}")
res = reg.search("he said HaHaHa")
print(res.group())  # HaHaHa
res = reg.search("he said HaHa")
print(res == None)  # True

# {x, y} - at leas x times, at most y times
# characters
# \d - any digit 0-9
# \D - any char that is not a numeric digit
# \w - any letter, numeric digit or underscore
# \W - any char that is not a letter, digit or undersocte
# \s - any space, tab or newline
# \S - any char that is not a space, tabl or newline